class apt_cacher_ng::params {
  include stdapp

  $app_dir = "${stdapp::base_dir}/apt-cacher-ng"
  $service_prefix = 'docker-compose@apt-cacher-ng'

  $value = parseyaml(file('apt_cacher_ng/params.yml'))
  $settings = std::nv($value['apt_cacher_ng::settings'], Hash, {})
}
