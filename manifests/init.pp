class apt_cacher_ng(
  Variant[String, Boolean] $ensure = present,
  Variant[String, Boolean] $service_ensure = running,
  Optional[Hash] $settings = {},
) inherits apt_cacher_ng::params {
  notice('---')

  $app_settings = $apt_cacher_ng::params::settings + std::nv($settings, Hash, {})
  info(std::pp({'app_settings' => $app_settings}))

  $file_ensure = std::file_ensure($ensure)
  $dir_ensure = std::dir_ensure($ensure)

  contain apt_cacher_ng::install
  contain apt_cacher_ng::config
  contain apt_cacher_ng::service

  case $ensure {
    'absent', false: {
      # absent の場合は disable service を先に行う
      Class[apt_cacher_ng::service]
      -> Class[apt_cacher_ng::install]
      -> Class[apt_cacher_ng::config]
    }
    default: {
      Class[apt_cacher_ng::install]
      -> Class[apt_cacher_ng::config]
      -> Class[apt_cacher_ng::service]
    }
  }
}

class apt_cacher_ng::install {
  stdapp::service::install { apt_cacher_ng: }
}

class apt_cacher_ng::config {
  #//empty
}

class apt_cacher_ng::service {
  stdapp::service { apt_cacher_ng: }
}
